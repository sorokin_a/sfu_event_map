﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraControll : MonoBehaviour
{
    public GameObject camera;

    public float max_z;
    public float min_z;
    public float max_x;
    public float min_x;
    public float max_y;
    
    private bool moving = false;
    private Vector3 cursorePositionOld;

    void Start()
    {
        setCameraY();
        setCameraAngles();
    }

    // Update is called once per frame
    void Update()
    {
        //camera Zoom
        float zoom = Input.GetAxis("Mouse ScrollWheel");
        if (zoom != 0) cameraZoom(zoom);

        //camera Move
        if (Input.GetMouseButtonDown(0))
        {
            moving = true;
            cursorePositionOld = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0)) moving = false;
        if (moving) cameraMove();
        Debug.Log("x = " + camera.transform.position.x.ToString());
        Debug.Log("z = " + camera.transform.position.z.ToString());

    }
    //y(x)=1/10*x^2+2
    void cameraZoom(float zoomStep)
    {
        float z = camera.transform.localPosition.z;

        if(((zoomStep > 0) && (z < -7)) || ((zoomStep < 0) && (z > -45))){
            z = z + zoomStep;
            float y = (float)(0.05 * Math.Pow(z, 2));
            camera.transform.localPosition = new Vector3(0, y, z);
            setCameraAngles();
        }

    }

    void setCameraAngles()
    {
        float z = Math.Abs(camera.transform.localPosition.z);
        float y = camera.transform.localPosition.y;

        float alpha = (float)(Math.Atan(y / z) * 180 / Math.PI);

        camera.transform.localEulerAngles = new Vector3(alpha, 0);
    }

    void setCameraY()
    {
        float z = camera.transform.localPosition.z;
        float y = (float)(0.05 * Math.Pow(z, 2));

        camera.transform.localPosition = new Vector3(0, y, z);
    }

    void cameraMove()
    {
        Vector3 cursorPositioNew  = Input.mousePosition;   
        if(cursorePositionOld != cursorPositioNew)
        {
            Vector3 delta = cursorPositioNew - cursorePositionOld;
            transform.position = transform.position + new Vector3(delta.x * Time.deltaTime * -1, 0, delta.y * Time.deltaTime * -1);
            cursorePositionOld = cursorPositioNew;
        }
    }
}
